$(function () {

    $('*[data-confirm]').click(function (e) {
        var that = $(this);
        bootbox.confirm(that.data('confirm'), function (result) {
            if (result) {
                window.location.href = that.attr('href');
            }
        });
        return false;
    });

    $('.redactor_content').redactor({
	    wym: true,
        plugins: ['fullscreen'],
        imageUpload: '/cms/pages/upload'
    });


    $('.autogrow').autosize();


});