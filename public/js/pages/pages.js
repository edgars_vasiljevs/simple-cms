$(function() {

    var updateSlugs = function() {
        var list = $('#menu_sortable li.page-list-item');

        list.each(function(i, li) {

            li = $(li);

            var parents = li.parents('li.page-list-item').toArray().reverse();

            var slug = li.data('slug');

            if (slug == '/') {
                slug = '';
            }

            var parent_url = '';

            parents.each(function(parent, i) {

                var slug = $(parent).data('slug');

                if (slug !== '/') {
                    parent_url += slug + '/';
                }

            });

            var fullURL = '/' + parent_url + slug;

            li.find('> div.page-item .page-url').html( fullURL.truncate(30) ).attr('href', fullURL);

        });

    };

    updateSlugs();


    $('#menu_sortable').nestedSortable({
        handle: '.dragicon',
        items: 'li.sortable',
        toleranceElement: '> div',
        listType: 'ul',
        placeholder: 'menu-placeholder',
        helper:	'clone',
        isAllowed: function(item, parent) {

            if (item.data('root') !== undefined && parent) {
                return false;
            }

            return true;

        },
        relocate: function() {
           updateSlugs();
        }
    });




    $('#save_menu').click(function() {

        var data = Object.toQueryString($('#menu_sortable').nestedSortable('toHierarchy'));

        $.post('/admin/pages/save-order', data, function(data) {

            if (data.success) {

                $('div.alert').remove();

                var alert = $('<div class="alert alert-success" />').html('<strong>Success!</strong> Page order saved');

                $('#menu_sortable').before( alert );

                setTimeout(function() {
                    alert.fadeOut(300, function() {
                        alert.remove();
                    });
                }, 4000);

            }

        }, 'json');

        return false;

    });


});