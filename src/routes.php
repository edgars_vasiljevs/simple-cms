<?php

Route::filter('isAdmin', function () {
	if (!Auth::check()) {
		return Redirect::to('admin/auth');
	}
});

Route::controller('admin/auth', 'Simple\Cms\AuthController');

Route::group(array('before' => 'isAdmin'), function () {
	Route::controller('admin/pages', 'Simple\Cms\PagesController');
	Route::controller('admin/languages', 'Simple\Cms\LanguagesController');
	Route::controller('admin/translate/{to}', 'Simple\Cms\TranslateController');
	Route::controller('admin', 'Simple\Cms\HomeController');
});

