<?php namespace Simple\Cms;

use Config;
use Cache;

class TranslationHelper {

	/**
	 * Page types
	 */
	const PAGE_TYPE_STATIC = 1;
	const PAGE_TYPE_MODULE = 2;
	const PAGE_TYPE_CUSTOM = 3;

	/**
	 * Get translation based on current language
	 * @param string $string
	 * @param array $params
	 * @return string
	 */
	public static function getLang($string, $params = array()) {

		$translations = Cache::get('translations');

		// If first time, cache it
		if ( ! $translations) {
			$translations = self::cacheTranslations();
		}

		if ( ! isset($translations[Language::$default][$string] )) {

			$id = Translation::create(array(
				'string' => $string,
				'main' => 1,
				'code' => Language::$default,
				'section_type' => PublicController::$page->type,
				'section_id' => PublicController::$page->id,
			))->id;

			foreach(Language::$languages as $code => $lang) {
				Translation::create(array(
					'string' => $string,
					'parent_id' => $id,
					'code' => $code,
					'section_type' => PublicController::$page->type,
					'section_id' => PublicController::$page->id,
				));
			}

			$translations = self::cacheTranslations();

		}

		$current = Config::get('app.locale');

		return vsprintf($translations[$current][$string], $params);

	}


	/**
	 * Cache all translations
	 * @return array
	 */
	private static function cacheTranslations() {

		$translations = array();

		foreach(Language::$languages as $code => $language) {

			$lang_data = Translation::whereNotNull('parent_id')->where('code', '=', $code)->get();

			foreach($lang_data as $row) {
				$data[$code][$row->parent_id] = $row->string;
			}

		}

		$strings = Translation::getMain();

		foreach(Language::$languages as $code => $language) {

			foreach($strings as $row) {

				$translations[$code][$row->string] = empty($data[$code][$row->id])
					? $row->string
					: $data[$code][$row->id];

			}

		}

		Cache::forever('translations', $translations);

		return $translations;

	}

	/**
	 * Update translation cache
	 * @return array
	 */
	public static function updateCache() {
		return self::cacheTranslations();
	}



}