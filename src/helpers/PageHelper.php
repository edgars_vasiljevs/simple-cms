<?php namespace Simple\Cms;

use Request;
use Route;
use App;
use Whoops\Example\Exception;

class PageHelper {

	/**
	 * Initialize pages routes
	 */
	public static function loadPublicRoutes() {

		$pages = Page::getPageTree();


		$locale = Request::segment(1);

		if (isset(Language::$languages[$locale])) {
			App::setLocale($locale);
		} else {
			$locale = NULL;
		}

		Route::group(array('prefix' => $locale), function () use ($pages) {
			foreach ($pages as $page) {

				// Route a static page
				if ($page->type == Page::TYPE_STATIC) {
					Route::get($page->url_full, 'PublicController@textPage');
				}

				// Route a custom page
				if ($page->type == Page::TYPE_CUSTOM) {

					$methodGet = 'get' . ucfirst( CustomPage::getPageMethod($page->custom_page_id) );
					$methodPost = 'post' . ucfirst( CustomPage::getPageMethod($page->custom_page_id) );

					Route::get($page->url_full, 'PublicController@' . $methodGet );
					Route::post($page->url_full, 'PublicController@' . $methodPost );

				}

				// Route a custom page
				if ($page->type == Page::TYPE_MODULE) {

					$module = Module::find( $page->module_id );

					if (is_null($module)) {
						throw new Exception('Module not found');
					}

					if (Page::hasChildren( $page->id )) {
						Route::get($page->url_full, $module->package . '\DefaultController@getIndex' );
					}
					else {
						Route::controller($page->url_full, $module->package . '\DefaultController' );
					}

				}

			}

		});

	}




	/**
	 * Renders array of pages to HTML
	 * @param array $array
	 * @return string $result
	 */
	public static function renderMenu($array) {

		$result = '';

		foreach($array as $row) {

			$result .= '<li class="page-list-item sortable" id="item_' . $row['id'] . '" data-slug="' . $row['url'] . '" ' . ($row['home']==1 ? 'data-root="'. $row['id'] .'"' : '') . '>
							<div class="page-item clearfix">
								<span class="dragicon"></span>
								<h4>' . \Str::limit($row['title']->cms, 30) . '</h4>

								<div class="page-options">
									<div class="btn-group2">

										<span class="label '.($row['status'] == 1 ? 'label-success' : '').'">'.($row['status'] == 1 ? 'Enabled' : 'Disabled').'</span>
										<span class="label '.($row['menu'] == 1 ? 'label-success' : '').'">'.($row['menu'] == 1 ? 'Visible' : 'Hidden').'</span>

										<a href="/admin/pages/edit/' . $row['id'] . '" class="btn"><i class="icon-edit"></i> Edit</a>
										' . ($row['home']
											? '<a disabled="disabled" href="javascript:;" class="btn btn-danger"><i class="icon-remove icon-white"></i></a>'
											: '<a href="/admin/pages/delete/' . $row['id'] . '" data-confirm="Are you sure you want to delete this page?" class="btn btn btn-danger"><i class="icon-remove icon-white"></i></a>'
										) . '

									</div>

									<span class="page-type">' . Page::getTypeName($row) . '</span>
								</div>

								<a href="" class="page-url" target="_blank"></a>

							</div>';

			if (array_key_exists('children', $row)) {
				$result .= "<ul>" . PageHelper::renderMenu($row['children']) . "</ul>";
			}

			$result .= '</li>';

		}

		return $result;

	}

}




?>