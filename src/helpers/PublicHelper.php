<?php namespace Simple\Cms;

/**
 * Public methods for views
 *
 * Class PublicHelper
 * @package Simple\Cms
 */
class PublicHelper {

	/**
	 * Shortcut for Page::$tree
	 * @return mixed
	 */
	public static function getTree() {
		return Page::$tree;
	}

	/**
	 * Get menu by depth
	 * @param int $depth
	 * @return array
	 */
	public static function getMenu($depth = 0) {
		return Page::getTreeByDepth($depth);
	}

	/**
	 * Get submenu by parent_id
	 * @param $parent_id
	 * @return array
	 */
	public static function getSubmenu($parent_id) {
		return Page::getTreeByParent($parent_id);
	}

	/**
	 * Get first child by parent_id
	 * @param $parent_id
	 * @return Page
	 */
	public static function getFirstChild($parent_id) {
		return Page::getFirstChild($parent_id);
	}

	/**
	 * Return current page
	 * @return Page
	 */
	public static function getCurrentPage() {
		return PublicController::$page;
	}

	/**
	 * Get array of path
	 */
	public static function getPathArray() {
		return Page::getPathArray();
	}

	/**
	 * Get active page by level
	 * @param $level
	 * @return mixed
	 */
	public static function getActiveByLevel($level = 0) {
		return Page::getAciveByLevel($level);
	}

	/**
	 * Get active languages
	 */
	public static function getLanguages() {
		return Language::$languages;
	}

	/**
	 * Get active languages
	 */
	public static function getCurrentLanguage() {
		return Language::$languages[ \Config::get('app.locale') ];
	}

	/**
	 * Get active languages
	 */
	public static function getLanguagesExcept($locale) {
		return array_filter(Language::$languages, function($item) use ($locale) {
			return $item->code != $locale;
		});
	}

	/**
	 * Get page by ID
	 */
	public static function getPageById($id) {
		foreach(self::getTree() as $item) {
			if ($item->id == $id) {
				return $item;
			}
		}
	}

}