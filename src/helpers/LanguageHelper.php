<?php namespace Simple\Cms;

use Config;
use DB;

class LanguageHelper {

	/**
	 * Initialize languages
	 */
	public static function loadLanguages() {

		$languages = Language::where('status', '=', 1)->get();

		$active_languages = array();

		Language::$languages = array();

		foreach($languages as $language) {
			Language::$languages[$language->code] = $language;
			$active_languages[] = $language->code;
		}

		Config::set('app.locale_default', Config::get('app.locale'));

		Language::$default = Config::get('app.locale');

	}

	/**
	 * Update all multifields
	 */
	public static function updateMultiFields() {

		// reload current languages
		self::loadLanguages();

		$classes = array_filter(get_declared_classes(), function ($v) {
			return strpos($v, __NAMESPACE__) !== FALSE;
		});

		foreach($classes as $class) {
			/** @var $multi string */

			if (isset($class::$multi) && is_subclass_of($class, 'Simple\Cms\Multi')) {
				self::updateClassMultiFields($class);
			}
		}

	}


	/**
	 * Update multi-language fields on single class
	 * @param object $class
	 */
	public static function updateClassMultiFields($class) {
		
		$multi = $class::$multi;
		$model = new $class;

		$locale = Config::get('cms::cms.locale');

		$select = array_merge(array('id'), $multi);

		$rows = $model->select($select)->get();

		foreach($rows as $row) {

			$new_data = array();

			foreach($multi as $field) {

				$data = $row->{$field};

				foreach(Language::$languages as $code => $lang) {

					if (empty($data->{$code})) {
						$new_data[$field][$code] = isset($data->{$locale}) ? $data->{$locale} : '';
					}
					else {
						$new_data[$field][$code] = $data->{$code};
					}

				}

				$new_data[$field] = json_encode($new_data[$field]);

			}

			DB::table($model->getTable())->where('id', '=', $row->id)->update($new_data);

		}


	}




}