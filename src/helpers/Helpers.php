<?php

/**
 * Shortcut to Translation::getLang
 *
 * @param $string
 * @param array $params
 * @return string
 */
function lang($string, $params = array()) {
	return \Simple\Cms\TranslationHelper::getLang($string, $params);
}

function str_compact($string) {
	return preg_replace('@[\s+]+@', " ", $string);
}


/**
 * Database type sort
 * @return mixed
 */
function array_orderby() {
	$args = func_get_args();
	$data = array_shift($args);
	foreach ($args as $n => $field) {
		if (is_string($field)) {
			$tmp = array();
			foreach ($data as $key => $row)
				$tmp[$key] = $row[$field];
			$args[$n] = $tmp;
		}
	}
	$args[] = &$data;
	call_user_func_array('array_multisort', $args);
	return array_pop($args);
}

/**
 * Sort flat array to parent-child order
 * @param array $els
 * @param int $parentID
 * @param array $result
 * @return array
 */
function parentChildSort($els, $parentID = 0, &$result = array()){

	foreach ($els as $key => $value) {
		if ($value['parent_id'] == $parentID){

			array_push($result, $value);

			unset($els[$key]);

			$oldParent = $parentID;
			$parentID = $value['id'];

			parentChildSort($els, $parentID, $result);

			$parentID = $oldParent;

		}
	}

	return $result;
}

/**
 * Strip specific tag
 * @param $tags
 * @param $string
 *
 * @return mixed
 */
function stripSingleTags($tags, $string) {
	foreach( $tags as $tag ) {
		$string = preg_replace('#</?'.$tag.'[^>]*>#is', '', $string);
	}
	return $string;
}

