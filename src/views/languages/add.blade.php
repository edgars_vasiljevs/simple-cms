<div class="page-header">
	<div class="pull-left">
		<h1>Add new language</h1>
	</div>

	<div class="pull-right">
		<ul class="minitiles">
			<li class="lightgrey">
				<a href="/admin/languages"><i class="icon-arrow-left"></i></a>
			</li>
		</ul>
	</div>

</div>

@if (Session::has('created') || Session::has('updated'))
<div class="alert alert-success compact">
	<button type="button" class="close" data-dismiss="alert">×</button>

	@if (Session::has('created'))
	<strong>Success!</strong> Language successfully created!
	@elseif (Session::has('updated'))
	<strong>Success!</strong> Language successfully saved!
	@endif

</div>
@endif


<div class="row-fluid">
	<div class="span12">
		<div class="box box-bordered">
			<div class="box-title">
				<h3><i class="icon-th-list"></i> New language</h3>
			</div>
			<div class="box-content nopadding">
				<form action="{{ isset($language) ? '/admin/languages/update/' . $language->id : '/admin/languages/create' }}" method="POST" class="form-horizontal form-bordered">


					<div class="control-group">
						<label class="control-label">
							Title <span class="required">*</span>
						</label>

						<div class="controls @if ($errors->has('title')) error @endif">
							<input type="text" name="title" value="{{ Input::old('title', isset($language) ? $language->title : '' ) }}" id="title" class="input-xlarge"/>

							@if ($errors->has('title'))
							<span class="help-inline error">{{ $errors->first('title') }}</span>
							@endif

						</div>

					</div>


					<div class="control-group">
						<label class="control-label">Status</label>

						<div class="controls">
							<label class="checkbox">

								{{ Form::hidden('status', false) }}
								{{ Form::checkbox('status', true, Input::old('status', isset($language->status) ? $language->status : true)) }} Enabled

							</label>
						</div>
					</div>


					<div class="control-group">
						<label class="control-label">
							Code <span class="required">*</span>
						</label>

						<div class="controls @if ($errors->has('code')) error @endif">
							<input type="text" name="code" value="{{ Input::old('code', isset($language) ? $language->code : '' ) }}" id="code" class="input-small"/>

							@if ($errors->has('code'))
							<span class="help-inline error">{{ $errors->first('code') }}</span>
							@endif

						</div>

						<div class="controls append">
							<span class="help-block">Language code. For example <code>en</code> for English</span>
						</div>

					</div>

					<div class="form-actions">
						<button type="submit" class="btn btn-primary">Save changes</button>
						<a href="/admin/languages" class="btn">Cancel</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

