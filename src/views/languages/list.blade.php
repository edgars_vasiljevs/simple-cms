<div class="page-header">
	<div class="pull-left">
		<h1>Languages</h1>
	</div>
	<div class="pull-right">
		<ul class="minitiles">
			<li class="satgreen">
				<a href="/admin/languages/new"><i class="icon-plus-sign"></i></a>
			</li>
		</ul>
	</div>
</div>

@if (Session::has('deleted'))
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>Success!</strong> Language successfully deleted!
</div>
@endif

<div class="row-fluid">
	<div class="span12">

		<div class="box">

			<div class="box-content nopadding">
				<table class="table table-hover table-nomargin table-bordered table-top-border">
					<thead>
						<tr>
							<th style="width: 30px;">Flag</th>
							<th>Language</th>
							<th style="width: 100px;">Code</th>
							<th style="width: 100px;">Status</th>
							<th style="width: 95px;">Options</th>

						</tr>
					</thead>
					<tbody>
						@foreach($languages as $lang)

							<tr>
								<td align="center"><img src="/packages/simple/cms/img/flags/{{ $lang->code }}.png" /></td>
								<td>{{ $lang->title }}</td>
								<td>{{ $lang->code }}</td>
								<td>{{ $lang->status == 1 ? '<span class="label label-success">Enabled</span>' : '<span class="label">Disabled</span>' }}</td>
								<td>
									<a class="btn" href="/admin/languages/edit/{{ $lang->id }}"><i class="icon-edit"></i> Edit</a>
									@if ( ! $lang->main)
									<a class="btn btn-danger" data-confirm="Are you sure you want to delete this language?" href="/admin/languages/delete/{{ $lang->id }}"><i class="icon-remove"></i></a>
									@endif
								</td>
							</tr>

						@endforeach

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
