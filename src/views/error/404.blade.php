<!doctype html>
<html>
<head>
	<title>SIMPLE CMS</title>
	<meta charset="utf8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent"/>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="/packages/simple/cms/css/bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="/packages/simple/cms/css/bootstrap-responsive.min.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="/packages/simple/cms/css/style.css">
	<!-- Redactor CSS -->
	<link rel="stylesheet" href="/packages/simple/cms/js/plugins/redactor/redactor.css">
	<!-- Custom CSS -->
	<link rel="stylesheet" href="/packages/simple/cms/css/custom.css">


	<!-- Favicon -->
	<link rel="shortcut icon" href="/packages/simple/cms/img/favicon.ico"/>
	<!-- Apple devices Homescreen icon -->
	<link rel="apple-touch-icon-precomposed" href="/packages/simple/cms/img/apple-touch-icon-precomposed.png"/>

</head>

<body class="error">

	<div class="wrapper">
		<div class="code"><span>404</span><i class="icon-warning-sign"></i></div>
		<div class="desc" style="text-align: center">{{ isset($message) ? $message : 'Oops! Sorry, that page could\'nt be found.' }}</div>
		<div class="buttons">
			<div class="text-center"><a href="javascript:history.back();" class="btn"><i class="icon-arrow-left"></i> Back</a></div>
		</div>
	</div>


</body>

</html>




