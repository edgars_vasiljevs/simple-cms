<div class="page-header">
	<div class="pull-left">
		<h1>Translate</h1>
	</div>
</div>


@if (Session::has('saved'))
<div class="alert alert-success compact">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>Success!</strong> Translations saved successfully!
</div>
@endif


<div class="row-fluid">
	<div class="span6">
		<div class="box lang-btn-box">
			<div class="box-title">
				<h3>
					<i class="icon-flag"></i>
					Translate from
				</h3>
			</div>
			<div class="box-content">

				<a class="btn btn-warning" href=""><img src="/packages/simple/cms/img/flags/en.png" alt=""/> English</a>

			</div>
		</div>

	</div>
	<div class="span6">
		<div class="box lang-btn-box">
			<div class="box-title">
				<h3>
					<i class="icon-flag"></i>
					Translate to
				</h3>
			</div>
			<div class="box-content">

				@foreach( Simple\Cms\Language::$languages as $lang )
					<a class="btn{{ $to == $lang->code ? ' btn-warning' : '' }}" href="/admin/translate/{{ $lang->code }}"><img src="/packages/simple/cms/img/flags/{{ $lang->code }}.png" alt=""/> {{ $lang->title }}</a>
				@endforeach

			</div>
		</div>

	</div>
</div>

<form action="" method="post">

	{{ Form::hidden('to', $to) }}

	@if ( ! empty($strings))

		@foreach($strings as $string)

	<div class="row-fluid translate">
		<div class="span6">
			<div class="box">
				<div class="box-content">
					<div class="original">{{ str_replace(array('__CODE__', '__/CODE__'), array('<code>', '</code>'), e(preg_replace('/(<[^<>]+>)/i', '__CODE__\1__/CODE__', str_compact($string->string)))) }}</div>
				</div>
			</div>
		</div>
		<div class="span6">
			<div class="box">
				<div class="box-content">
					<textarea class="input-block-level autogrow translated" name="translations[{{ $string->id }}]" id="" cols="30" rows="1">{{ isset($translations[$string->id]) ? str_compact($translations[$string->id]) : '' }}</textarea>
				</div>
			</div>
		</div>
	</div>


		@endforeach


	@endif

	<div class="form-actions">
		<button type="submit" class="btn btn-primary pull-right">Save changes</button>
	</div>
</form>