<!doctype html>
<html>
<head>
	<title>SIMPLE CMS</title>
	<meta charset="utf8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent"/>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="/packages/simple/cms/css/bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="/packages/simple/cms/css/bootstrap-responsive.min.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="/packages/simple/cms/css/style.css">
	<!-- Redactor CSS -->
	<link rel="stylesheet" href="/packages/simple/cms/js/plugins/redactor/redactor.css">
	<!-- Custom CSS -->
	<link rel="stylesheet" href="/packages/simple/cms/css/custom.css">

	<!-- jQuery -->
	<script src="/packages/simple/cms/js/jquery.min.js"></script>

	<!-- SugarJS -->
	<script src="/packages/simple/cms/js/sugar-1.3.9.min.js"></script>

	<!-- CKEditor -->
	<script src="/packages/simple/cms/js/redactor.js"></script>
	<script src="/packages/simple/cms/js/plugins/redactor/fullscreen.js"></script>

	<!-- Bootstrap -->
	<script src="/packages/simple/cms/js/bootstrap.min.js"></script>

	<!-- Bootstrap file upload -->
	<script src="/packages/simple/cms/js/bootstrap-fileupload.js"></script>

	<!-- Bootbox -->
	<script src="/packages/simple/cms/js/plugins/bootbox/jquery.bootbox.js"></script>

	<!-- Textarea autogrow -->
	<script src="/packages/simple/cms/js/jquery.autosize.min.js"></script>

	<!-- Custom scripts -->
	<script src="/packages/simple/cms/js/custom.js"></script>

	@if (isset($scripts))
	@foreach ($scripts as $script)
	<script src="/packages/simple/cms/js/{{ $script }}.js"></script>
	@endforeach
	@endif


	<!-- Favicon -->
	<link rel="shortcut icon" href="/packages/simple/cms/img/favicon.ico"/>
	<!-- Apple devices Homescreen icon -->
	<link rel="apple-touch-icon-precomposed" href="/packages/simple/cms/img/apple-touch-icon-precomposed.png"/>

</head>

<body>
<div id="navigation">
	<div class="container-fluid">
		<a href="/admin/" id="brand">SIMPLECMS</a>

		<div class="user">
			<div class="dropdown">
				<a href="#" class='dropdown-toggle' data-toggle="dropdown">{{ Auth::user()->fullname }} <img
						src="/packages/simple/cms/img/demo/user-avatar.jpg" alt=""></a>
				<ul class="dropdown-menu pull-right">
					<li>
						<a href="more-userprofile.html">Edit profile</a>
					</li>
					<li>
						<a href="#">Account settings</a>
					</li>
					<li>
						<a href="/admin/auth/logout">Sign out</a>
					</li>
				</ul>
			</div>
		</div>

	</div>
</div>
<div class="container-fluid" id="content">
	<div id="left">
		<form action="http://www.eakroko.de/flat/search-results.html" method="GET" class='search-form'>
			<div class="search-pane">
				<input type="text" name="search" placeholder="Search here...">
				<button type="submit"><i class="icon-search"></i></button>
			</div>
		</form>

		<div class="subnav">
			<ul class="subnav-menu">
				<li>
					<a href="/admin/">Dashboard</a>
				</li>
			</ul>
		</div>
		<div class="subnav">
			<div class="subnav-title">
				<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Content</span></a>
			</div>
			<ul class="subnav-menu">
				<li>
					<a href="/admin/pages">Pages</a>
				</li>
				<li>
					<a href="/admin/translate/{{ \Config::get('app.locale') }}">Translate</a>
				</li>
				<li>
					<a href="/admin/banners">Banners</a>
				</li>

			</ul>
		</div>
		<div class="subnav">
			<div class="subnav-title">
				<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Settings</span></a>
			</div>
			<ul class="subnav-menu">
				<li>
					<a href="/admin/languages">Languages</a>
				</li>
				<li>
					<a href="#">Settings</a>
				</li>
				<li>
					<a href="#">Users</a>
				</li>
			</ul>
		</div>
	</div>
	<div id="main">
		<div class="container-fluid">

			{{ $content }}

		</div>
	</div>
</div>

</body>

</html>

