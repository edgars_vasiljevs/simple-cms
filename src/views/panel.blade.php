<div class="panel">

	<div class="header">
		<div>
			<h3>SimpleCMS
				<small>1.0</small>
			</h3>
		</div>
		<div class="username right">
			You are logged as: <strong>{{ Auth::user()->email }}</strong><br/>
			<a href="">Profile</a> &nbsp;
			<a href="">Help</a> &nbsp;
			<a href="{{ URL::to('admin/auth/logout') }}">Logout</a>
		</div>
	</div>


	<ul class="tabs right">
		<li
		@if (Request::segment(2) == '' OR Request::segment(2) == 'home') class="current" @endif><a
			href="{{ URL::to('admin') }}"><i class="icon-home"></i> Home</a></li>
		<li
		@if (Request::segment(2) == 'pages') class="current" @endif><a href="{{ URL::to('admin/pages') }}"><i
				class="icon-laptop"></i> Pages</a></li>
		<li
		@if (Request::segment(2) == 'menu') class="current" @endif><a href="{{ URL::to('admin/menu') }}"><i
				class="icon-reorder"></i> Menu</a></li>
		<li
		@if (Request::segment(2) == 'users') class="current" @endif><a href="{{ URL::to('admin/users') }}"><i
				class="icon-user"></i> Users</a></li>
		<li
		@if (Request::segment(2) == 'settings') class="current" @endif><a href="{{ URL::to('admin/settings') }}"><i
				class="icon-cog"></i> Settings</a></li>
	</ul>
	<div class="wb">
		{{ $content }}
	</div>
</div>