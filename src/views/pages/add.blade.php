<div class="page-header">
	<div class="pull-left">
		<h1>Add new page</h1>
	</div>

	<div class="pull-right">
		<ul class="minitiles">
			<li class="lightgrey">
				<a href="/admin/pages"><i class="icon-arrow-left"></i></a>
			</li>
		</ul>
	</div>

</div>

@if (Session::has('created') || Session::has('updated'))
<div class="alert alert-success compact">
	<button type="button" class="close" data-dismiss="alert">×</button>

	@if (Session::get('created'))
		<strong>Success!</strong> Page successfully created!
	@elseif (Session::get('updated'))
		<strong>Success!</strong> Page successfully saved!
	@endif
</div>
@endif


<div class="row-fluid">
	<div class="span12">
		<div class="box box-bordered">
			<div class="box-title">
				<h3><i class="icon-th-list"></i> New page</h3>
			</div>
			<div class="box-content nopadding">
				<form action="{{ isset($page) ? '/admin/pages/update/' . $page->id : '/admin/pages/create' }}" method="POST" class="form-horizontal form-bordered">

					<div class="control-group">
						<label for="title" class="control-label">
							Title <span class="required">*</span>
						</label>

						@foreach(Simple\Cms\Language::$languages as $lang)

						<div class="controls append @if ($errors->has('title_' . $lang->code)) error @endif ">
							<div class="input-append">
								<input type="text" name="title_{{ $lang->code }}" value="{{ Input::old('title_' . $lang->code, isset($page) ? $page->title->{$lang->code} : '' ) }}" id="title" class="input-xlarge"/>
								<span class="add-on"><img src="/packages/simple/cms/img/flags/{{ $lang->code }}.png" alt="{{ $lang->title }}"/> {{ $lang->title }}</span>
							</div>
							@if ($errors->has('title_' . $lang->code))
							<span class="help-inline error">{{ $errors->first('title_' . $lang->code) }}</span>
							@endif
						</div>

						@endforeach

						<div class="controls append">
							<span class="help-block">Title for the menu</span>
						</div>

					</div>
					<div class="control-group">
						<label for="url" class="control-label">URL slug</label>

						<div class="controls">
							<div class="input-prepend">

								<input {{ isset($page) && $page->home ? 'disabled="disabled"' : '' }} type="text" name="url" value="{{ Input::old('url', isset($page) ? $page->url : '') }}" id="url" class="input-xlarge">
							</div>
							<span class="help-block">Part of URL corresponding to this page. Leave blank to auto-generate</span>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Status</label>

						<div class="controls">
							<label class="checkbox">

								{{ Form::hidden('status', isset($page->status) ? $page->status : true) }}
								{{ Form::checkbox('status', true, Input::old('status', isset($page->status) ? $page->status : true), isset($page) && $page->home ? array('disabled'=>'disabled') : array() ) }} Enabled

							</label>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Show in menu</label>

						<div class="controls">
							<label class="checkbox">

								{{ Form::hidden('menu', false) }}
								{{ Form::checkbox('menu', true, Input::old('menu', isset($page->menu) ? $page->menu : true)) }} Show

							</label>
						</div>
					</div>


					<div class="control-group">
						<label for="layout" class="control-label">Layout</label>

						<div class="controls">
							<select name="layout_id" id="layout">
								@foreach (Simple\Cms\Layout::all() as $layout)
								<option
								{{ (Input::old('layout_id') == $layout->id || (isset($page->layout_id) && $page->layout_id == $layout->id)) ? 'selected="selected"' : '' }} value="{{ $layout->id }}">{{ $layout->name }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="control-group">
						<label for="type" class="control-label">Page type</label>

						<div class="controls">
							<select name="type" id="type">
								<option value="1">Text Page</option>
								<option {{ (Input::old('type') == 2 || (isset($page->type) && $page->type == 2)) ? 'selected="selected"' : '' }} value="2">Module</option>
								<option {{ (Input::old('type') == 3 || (isset($page->type) && $page->type == 3)) ? 'selected="selected"' : '' }} value="3">Custom page</option>
							</select>
						</div>
					</div>
					<div id="content_content" class="control-group">
						<label class="control-label">Content</label>

						<div class="controls">

							<ul class="nav nav-tabs compact">
								@foreach(Simple\Cms\Language::$languages as $i => $lang)
								<li @if ($i == \Config::get('app.locale')) class="active" @endif><a href="#content_{{ $lang->code }}" data-toggle="tab"><img src="/packages/simple/cms/img/flags/{{ $lang->code }}.png" alt="{{ $lang->title }}"/> {{ $lang->title }}</a></li>
								@endforeach
							</ul>

							<div class="tab-content">
								@foreach(Simple\Cms\Language::$languages as $i => $lang)
								<div class="tab-pane @if ($i == \Config::get('app.locale')) active @endif" id="content_{{ $lang->code }}">
									<textarea name="content_{{$lang->code}}" class="redactor_content" rows="10">@if (Input::old('content_' . $lang->code )){{ Input::old('content_' . $lang->code) }}@elseif (isset($page) && $page->type == 1){{ $page->content->{ $lang->code } }}@endif</textarea>
								</div>
								@endforeach
							</div>
						</div>
					</div>

					<div id="content_module" class="control-group" style="display: none">
						<label for="module" class="control-label">Module</label>

						<div class="controls">
							<select id="module" name="module_id">
								@foreach($modules as $module)
									<option {{ isset($page) && $page->module_id == $module->id ? 'selected="selected"' : '' }} value="{{ $module->id }}">{{ $module->title }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div id="content_custom" class="control-group" style="display: none">
						<label for="custom_page" class="control-label">Custom page</label>

						<div class="controls">
							<select id="custom_page" name="custom_page_id">
								@foreach($custom as $custom_page)

									<option {{ (Input::old('custom_page_id') == $custom_page->id || (isset($page) && $page->custom_page_id == $custom_page->id)) ? 'selected="selected"' : '' }} value="{{ $custom_page->id }}">{{ $custom_page->title }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-actions">
						<button type="submit" class="btn btn-primary">Save changes</button>
						<a href="/admin/pages" class="btn">Cancel</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


<script>
	$(function () {

		$('#type').change(function (val) {

			var val = $(this).val();

			if (val == 1) {
				$('#content_custom, #content_module').hide();
				$('#content_content').show();
			}
			else if (val == 2) {
				$('#content_custom, #content_content').hide();
				$('#content_module').show();
			}
			else if (val == 3) {
				$('#content_module, #content_content').hide();
				$('#content_custom').show();
			}

		}).change();







	});
</script>