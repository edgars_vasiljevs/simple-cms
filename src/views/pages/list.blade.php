<div class="page-header">
	<div class="pull-left">
		<h1>Pages</h1>
	</div>
	<div class="pull-right">
		<ul class="minitiles">
			<li class="satgreen">
				<a href="/admin/pages/new"><i class="icon-plus-sign"></i></a>
			</li>
		</ul>
	</div>
</div>

@if (Session::has('deleted'))
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>Success!</strong> Page successfully deleted!
</div>
@endif

<div class="row-fluid">
	<div class="span12">

		<div class="box">
			<div class="box-content nopadding">

				<ul class="sortable-menu sortable ui-sortable" id="menu_sortable">

					{{ Simple\Cms\PageHelper::renderMenu($pages) }}

				</ul>

				<div class="form-actions-basic">
					<button type="submit" class="btn btn-primary" id="save_menu">Save</button>
				</div>

			</div>
		</div>
	</div>
</div>
