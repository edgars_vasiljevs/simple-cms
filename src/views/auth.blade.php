<!doctype html>
<html>
<head>
	<meta charset="utf8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent"/>


	<!-- Bootstrap -->
	<link rel="stylesheet" href="/packages/simple/cms/css/bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="/packages/simple/cms/css/bootstrap-responsive.min.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="/packages/simple/cms/css/style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="/packages/simple/cms/css/themes.css">


	<!-- jQuery -->
	<script src="/packages/simple/cms/js/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="/packages/simple/cms/js/bootstrap.min.js"></script>
	<script src="/packages/simple/cms/js/eakroko.js"></script>

	<!-- Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico"/>
	<!-- Apple devices Homescreen icon -->
	<link rel="apple-touch-icon-precomposed" href="/packages/simple/cms/img/apple-touch-icon-precomposed.png"/>

</head>

<body class='login'>
<div class="wrapper">
	<h1><a href="/admin/"><img src="/packages/simple/cms/img/logo-big.png" alt="" class='retina-ready' width="59"
	                           height="49">SIMPLECMS</a></h1>

	<div class="login-body">
		<h2>SIGN IN</h2>

		<form action="" method="post">
			<div class="email">
				<input type="text" name='username' placeholder="Email address" class='input-block-level'>
			</div>
			<div class="pw">
				<input type="password" name="password" placeholder="Password" class='input-block-level'>
			</div>
			<div class="submit">
				<input type="submit" value="Sign me in" class='btn btn-primary'>
			</div>
		</form>
		<div class="forget">
			<a href="#"><span>Forgot password?</span></a>
		</div>
	</div>
</div>

</body>

</html>
