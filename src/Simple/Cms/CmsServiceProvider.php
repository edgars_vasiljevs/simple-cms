<?php namespace Simple\Cms;

use Config;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class CmsServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = FALSE;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot() {

		$this->package('simple/cms');

		// Set default database
		Config::set('database.connections', Config::get('cms::cms.connections'));

		// Load languages
		LanguageHelper::loadLanguages();

		// Load CMS aliases
		AliasLoader::getInstance( Config::get('cms::cms.aliases') );

		// Load providers
		self::loadProviders($this, Config::get('cms::cms.providers'));

		// Load helper global helper functions
		include __DIR__ . '/../../helpers/Helpers.php';

		// Load CMS routes
		include __DIR__ . '/../../routes.php';

		// Load public routes
		PageHelper::loadPublicRoutes();

		// Error page
//		App::missing(function($exception) {
//			return \Response::view('cms::error.404', array(), 404);
//		});

	}

	/**
	 * Load CMS depended providers and aliases
	 */
	public static function loadProviders($app, $providers) {

		foreach($providers as $provider) {

			$provider = new $provider($app->app);

			$app->app->register( $provider );

			$provider->boot();

		}

	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register() {

	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides() {
		return array();
	}

}