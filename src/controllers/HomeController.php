<?php namespace Simple\Cms;

use Illuminate\Support\Facades\Response;
use Symfony\Component\CssSelector\Exception\ExpressionErrorException;
use \View;

class HomeController extends BaseController {

	/**
	 * Layout
	 */
	protected $layout = 'cms::template';


	/**
	 * Default view for admin panel
	 */
	public function getIndex() {

		$this->layout->content = View::make('cms::dashboard.dashboard');

	}

//	public function missingMethod($test) {
//
//		return \Response::view('cms::error.404', array('message' => 'Missing method'), 404);
//
//	}

}