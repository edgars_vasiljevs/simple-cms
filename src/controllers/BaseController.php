<?php namespace Simple\Cms;

use Controller;
use View;


class BaseController extends Controller {

	/**
	 * Set up layout for controller
	 */
	protected function setupLayout() {
		if (!is_null($this->layout)) {
			$this->layout = \View::make($this->layout);
		}
	}

}