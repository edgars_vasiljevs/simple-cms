<?php namespace Simple\Cms;

use Request;
use View;
use Whoops\Example\Exception;

class PublicController extends Controller {


	/**
	 * Current page object
	 * @var
	 */
	public static $page = NULL;


	/**
	 * Array of pages you are currently in
	 * @var array
	 */
	public static $path = array();


	/**
	 * Template
	 * @var \Illuminate\View\View|string
	 */
	public $template = 'template';

	/**
	 * Constructor
	 */
	public function __construct() {

		$pages = Page::getPageTree();

		usort($pages, function($a, $b) {
			return  $b["level"]- $a["level"];
		});

		foreach ($pages as $page) {

			$requestUri = Request::getRequestUri();

			if (preg_match('/^\/(?<lang>[a-z]{2})\/?/i', $requestUri, $match)
				&& isset(Language::$languages[$match['lang']])) {

				$requestUri = substr($requestUri, 3);

				if ( ! $requestUri) {
					$requestUri = '/';
				}

			}

			$url = substr($page->url_full, 0, 1) === '/' ? $page->url_full : '/' . $page->url_full;

			if ($url === $requestUri) {
				self::$page = $page;
				break;
			}

			if (strpos($requestUri, $url) === 0 && $url !== '/') {
				self::$page = $page;
				break;
			}

		}

		// set current page in global tree
		Page::setCurrentPage( self::$page->id );

		if (empty(self::$page)) {
			throw new Exception('Page not found');
		}

		$template = View::make($this->template);

		$layout = Layout::find(self::$page->layout_id);
		$layout = View::make($layout->filename);

		$layout->page = self::$page;
		$template->page = self::$page;

		$template->layout = $layout;

		$this->template = $template;

	}



}