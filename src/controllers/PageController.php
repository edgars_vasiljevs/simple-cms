<?php namespace Simple\Cms;

use View;
use Redirect;
use Input;
use Exception;


class PagesController extends BaseController {

	/**
	 * Layout
	 */
	protected $layout = 'cms::template';

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	/**
	 * Default view for admin panel
	 */
	public function getIndex() {

		$list = View::make('cms::pages.list');

		$list->pages = Page::getNested();

		$this->layout->content = $list;
		$this->layout->scripts = array('jquery-ui-1.10.1.custom.min', 'jquery.mjs.nestedSortable', 'pages/pages');

	}


	/**
	 * Add Page form
	 */
	public function getNew() {

		$view = View::make('cms::pages.add');

		$view->custom = CustomPage::all();
		$view->layouts = Layout::all();
		$view->modules = Module::all();

		$this->layout->content = $view;

	}

	/**
	 * Edit page
	 */
	public function getEdit($page_id = NULL) {

		$page = Page::find($page_id);

		if (is_null($page)) {
			throw new Exception('Page not found');
		}

		$view = View::make('cms::pages.add');

		$view->custom = CustomPage::all();
		$view->layouts = Layout::all();
		$view->modules = Module::all();
		$view->page = $page;

		$this->layout->content = $view;

	}

	/**
	 * Handle page add or edit
	 */
	public function postCreate() {

		$input = Input::all();

		$validation = new Validation\Page($input);

		if ($validation->passes()) {

			$page = Page::create($input);

			return Redirect::to('admin/pages/edit/' . $page->id)->with('created', TRUE);

		}

		return Redirect::to('admin/pages/new')->withInput()->withErrors($validation->errors);

	}

	/**
	 * Handle update
	 */
	public function postUpdate($page_id = NULL) {

		$input = Input::all();

		$validation = new Validation\Page($input);

		$page = Page::find($page_id);

		if (is_null($page)) {
			throw new Exception('Page not found');
		}

		if ($validation->passes()) {

			$page->update($input);

			return Redirect::to('admin/pages/edit/' . $page_id)->with('updated', TRUE);

		}

		return Redirect::to('admin/pages/edit/' . $page_id)->withInput()->withErrors($validation->errors);

	}

	/**
	 * Delete page
	 */
	public function getDelete($page_id) {

		$page = Page::find($page_id);

		if (is_null($page)) {
			throw new Exception('Page not found');
		}

		if ($page->home) {
			throw new Exception('Can\'t delete home page');
		}

		$page->delete();

		return Redirect::to('admin/pages')->with('deleted', TRUE);

	}

	/**
	 * Save order of pages
	 */
	public function postSaveOrder() {

		return json_encode(array(
			'success' => Page::savePageOrder( Input::all() )
		));

	}


}