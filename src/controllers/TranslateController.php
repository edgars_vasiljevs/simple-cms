<?php namespace Simple\Cms;

use View;
use Redirect;
use Input;
use Exception;

class TranslateController extends BaseController {

	/**
	 * Layout
	 */
	protected $layout = 'cms::template';

	/**
	 * Default view
	 */
	public function getIndex( $to = NULL) {

		$view = View::make('cms::translate.list');

		$view->to = $to;

		if (isset(Language::$languages[$to])) {

			$view->strings = Translation::getMain();

			$data = Translation::getByLanguageCode($to);

			$translations = array();

			foreach($data as $row) {
				$translations[$row->parent_id] = $row->string;
			}

			$view->translations = $translations;

		}

		$this->layout->content = $view;

	}

	/**
	 * Save translations
	 */
	public function postIndex($to = null) {

		$translations = Input::get('translations');

		$status = Translation::updateTranslations( $translations, $to );

		return Redirect::to('admin/translate/' . $to)->with('saved', true);

	}

}