<?php namespace Simple\Cms;

use \View;
use \Input;
use \Auth;
use \Redirect;
use \Hash;

class AuthController extends BaseController {

	/**
	 * Layout
	 */
	protected $layout = 'cms::auth';

	/**
	 * Default view for admin panel
	 */
	public function getIndex() {

		//$this->layout->content = View::make('cms::auth');

	}

	/**
	 * Handle logout
	 */
	public function getLogout() {

		Auth::logout();

		return Redirect::to('admin/auth');

	}


	/**
	 * Handle auth
	 */
	public function postIndex() {

		$credentials = array('email' => Input::get('username'), 'password' => Input::get('password'),);

		if (Auth::attempt($credentials)) {
			return Redirect::to('admin');
		} else {
			return Redirect::to('admin/auth')->withInput()->with('error', TRUE);
		}

	}

}

