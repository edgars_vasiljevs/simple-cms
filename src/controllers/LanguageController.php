<?php namespace Simple\Cms;

use View;
use Redirect;
use Input;
use Exception;

class LanguagesController extends BaseController {

	/**
	 * Layout
	 */
	protected $layout = 'cms::template';


	/**
	 * Default view
	 */
	public function getIndex() {

		$list = View::make('cms::languages.list');

		$list->languages = Language::all();

		$this->layout->content = $list;

	}

	/**
	 * Add language
	 */
	public function getNew() {

		$view = View::make('cms::languages.add');

		$this->layout->content = $view;

	}

	/**
	 * Edit language
	 * @param integer $language_id
	 */
	public function getEdit($language_id = NULL) {

		$language = Language::find($language_id);

		if (is_null($language)) {
			throw new Exception('Language not found');
		}

		$view = View::make('cms::languages.add');

		$view->language = $language;

		$this->layout->content = $view;

	}

	/**
	 * Delete a language
	 * @param integer $language_id
	 * @throws \Exception
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function getDelete($language_id = NULL) {

		$language = Language::find($language_id);

		if (is_null($language)) {
			throw new Exception('Language not found');
		}

		if ($language->main) {
			throw new Exception('Can\'t delete default language');
		}

		$code = $language->code;

		$language->delete();

		// remove language from multi fields
		LanguageHelper::updateMultiFields();

		// remove all translations
		Translation::removeByCode($code);

		// Update translation cache
		Translation::updateCache();


		return Redirect::to('admin/languages')->with('deleted', TRUE);

	}


	/**
	 * Saving language
	 */
	public function postCreate() {

		$input = Input::all();

		$validation = new Validation\Language($input);

		if ($validation->passes()) {

			$language = Language::create($input);

			// Add language to all multifields
			LanguageHelper::updateMultiFields();

			// Add language to translation
			Translation::addLanguage($language->code);

			// Update translation cache
			Translation::updateCache();

			return Redirect::to('admin/languages/edit/' . $language->id)->with('created', true);

		}

		return Redirect::to('admin/languages/new')->withInput()->withErrors($validation->errors);

	}

	/**
	 * Update language
	 */
	public function postUpdate($language_id = null) {

		$input = Input::all();

		$validation = new Validation\Language($input);

		$language = Language::find($language_id);

		if (is_null($language)) {
			throw new Exception('Language not found');
		}

		if ($validation->passes()) {

			$language->update($input);

			return Redirect::to('admin/languages/edit/' . $language_id)->with('updated', true);

		}

		return Redirect::to('admin/languages/edit/' . $language_id)->withInput()->withErrors($validation->errors);

	}


}