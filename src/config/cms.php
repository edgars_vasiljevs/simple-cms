<?php

/**
 * CMS config file
 */
return array(

	/**
	 * Default CMS locale
	 */
	'locale' => 'en',

	/**
	 * Database connections
	 */
	'connections' => array(
		'mysql' => array(
			'driver' => 'mysql',
			'host' => 'localhost',
			'database' => 'cms',
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
			'collation' =>
			'utf8_unicode_ci',
			'prefix' => 'simple_',
		),
	),

	/**
	 * Providers for CMS
	 */
	'providers' => array(
		
	),

	/**
	 * Global aliases from CMS
	 */
	'aliases' => array(
		'CMS'             => 'Simple\Cms\PublicHelper',
		'Controller'      => 'Simple\Cms\Controller', // monkey patch
	)

);