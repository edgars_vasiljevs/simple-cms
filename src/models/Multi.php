<?php namespace Simple\Cms;

use Eloquent;
use Monolog\Formatter;
use Config;

class Multi extends Eloquent {




	/**
	 * Convert multi fields
	 */
	public function __get($property) {

		if ( ! isset(static::$multi)) {
			return $this->getAttribute($property);
		}


		if (in_array($property, static::$multi) && is_object(json_decode($this->getAttribute($property)))) {

			$translations = json_decode($this->getAttribute($property));

			if (isset($translations->{ Config::get('cms::cms.locale') })) {
				$translations->cms = $translations->{ Config::get('cms::cms.locale') };
			}

			

			return new MultiOutput($translations);

		} else {
			return $this->getAttribute($property);
		}

	}

	/**
	 * Override default update for multi language support
	 */
	public function update(array $attributes = array()) {

		$attributes = self::convertedAttributes($attributes);

		return parent::update($attributes);

	}

	/**
	 * Override create function for multi language support
	 */
	public static function create(array $attributes = array()) {

		$attributes = self::convertedAttributes($attributes);

		return parent::create($attributes);

	}

	/**
	 * Empty onSave function
	 */
	public function onSave() {
		return TRUE;
	}

	/**
	 * Adds support for onSave function
	 */
	public function save(array $options = array()) {

		return $this->onSave() ? parent::save($options) : FALSE;

	}

	/**
	 * Get language
	 */
	public function getMultiAttribute($attribute, $language = NULL) {

		if (!$language) {
			$language = Config::get('app.locale');
		}

		return $this->{$attribute}->{$language};

	}

	/**
	 * Get mutl attribute for CMS
	 */
	public function getCMSAttribute($attribute, $language = NULL) {

		return $this->getMultiAttribute($attribute, Config::get('cms::cms.locale'));

	}


	/**
	 * Convert multi language attributes to json
	 */
	private static function convertedAttributes(array $attributes = array()) {


		$converted_attributes = array();

		foreach ($attributes as $key => $value) {

			foreach (Language::$languages as $lang) {

				if (preg_match('@^([a-z_]+)_' . $lang->code . '$@i', $key, $match)) {

					if (in_array($match[1], static::$multi)) {
						$converted_attributes[$match[1]][$lang->code] = $value;
					}

					unset($attributes[$key]);

				}

			}

		}

		array_walk($converted_attributes, function (&$value) {
			$value = json_encode($value);
		});

		return array_merge($attributes, $converted_attributes);

	}


}