<?php namespace Simple\Cms;

use Config;
use Cache;

class Translation extends Multi {

	/**
	 * Fillable fields
	 */
	protected  $fillable = array('string', 'code', 'parent_id', 'section_type', 'section_id');


	/**
	 * Disable timestamps
	 * @var bool
	 */
	public $timestamps = FALSE;

	/**
	 * Shortcut to TranslationHelper::updateCache
	 */
	public static function updateCache() {
		TranslationHelper::updateCache();
	}

	/**
	 * Get translations by country code
	 * @param $code
	 * @return mixed
	 */
	public static function getByLanguageCode($code) {
		return self::where('code', '=', $code)->whereNotNull('parent_id')->get();
	}

	/**
	 * Get translations for default language
	 * @return mixed
	 */
	public static function getDefaultLanguage() {
		$locale = Config::get('locale');
		return self::where('code', '=', $locale)->get();
	}

	/**
	 * Get main strings
	 * @return mixed
	 */
	public static function getMain() {
		return self::whereNull('parent_id')->get();
	}

	/**
	 * Update translations
	 * @return int
	 */
	public static function updateTranslations($data, $to) {

		foreach($data as $parent => $value) {

			$value = trim($value);

			if (empty($value)) {
				continue;
			}

			$input = array(
				'string' => $value,
				'code' => $to,
				'parent_id' => $parent,
			);

			$translation = Translation::where('code', '=', $to)->where('parent_id', '=', $parent)->first();

			if ( ! is_null($translation)) {
				$translation->update($input);
			}
			else {
				$translation = Translation::create($input);
			}

		}

		return $translation->id;

	}

	/**
	 * Remove all translations by language code
	 * @param string $code
	 * @return bool
	 */
	public static function removeByCode($code) {
		return self::where('code', '=', $code)->delete();
	}

	/**
	 * Add translation language
	 * @param string $code
	 */
	public static function addLanguage($code) {

		$default = self::whereNotNull('parent_id')->where('code', '=', Language::$default)->get();

		foreach($default as $row) {
			$insert = array(
				'parent_id' => $row->parent_id,
				'main' => 0,
				'code' => $code,
				'string' => $row->string
			);

			self::create($insert);

		}

	}

}


