<?php namespace Simple\Cms;

use Eloquent;

class CustomPage extends Eloquent {

	/**
	 * Table
	 */
	protected $table = 'custom_pages';

	/**
	 * List of custom pages
	 * @var array
	 */
	protected static $customPages = array();

	/**
	 * Get custom page by ID
	 * @param $id
	 * @return mixed
	 */
	public static function getPageMethod($id) {

		if (empty(self::$customPages)) {
			$pages = self::all();

			foreach($pages as $page) {
				self::$customPages[$page->id] = $page;
			}
		}

		return isset(self::$customPages[$id]) ? self::$customPages[$id]->method : NULL;

	}


}