<?php namespace Simple\Cms;

use Cocur\Slugify\Slugify;
use Request;

class Page extends Multi {

	/**
	 * Table
	 */
	protected $table = 'pages';

	/**
	 * Page statuses
	 */
	const TYPE_STATIC = 1;
	const TYPE_MODULE = 2;
	const TYPE_CUSTOM = 3;


	/**
	 * Page statuses
	 */
	const STATUS_ENABLED = 1;
	const STATUS_DISABLED = 0;


	/**
	 * Page types
	 */
	public static $types = array(
		self::TYPE_STATIC => 'Text Page',
		self::TYPE_MODULE => 'Module',
		self::TYPE_CUSTOM => 'Custom Page',
	);

	/**
	 * Multi fields
	 */
	public static $multi = array('title', 'content');


	/**
	 * Fillable fields
	 */
	protected $fillable = array('title', 'url', 'status', 'layout_id', 'menu', 'type', 'content', 'module_id', 'custom_page_id');

	/**
	 * Page tree
	 * @var
	 */
	public static $tree;

	/**
	 * Load page tree for later use
	 */
	private static function loadPageTree() {

		$pages = Page::where('status', '=', static::STATUS_ENABLED)->orderBy('ord', 'ASC')->get();

		$tree = array();

		foreach($pages as $page) {

			$item = $page;

			$item->url_full = self::getPageFullURL($pages, $page, $page->url);
			$item->level = self::getTreeLevel($pages, $page);
			$item->active = self::isPageActive($page);
			$item->current = FALSE;

			$tree[] = $item;

		}

		self::$tree = parentChildSort($tree);

	}

	/**
	 * Get page type name
	 * @param Page $page
	 * @return mixed
	 */
	public static function getTypeName($page) {

		if ($page['type'] == self::TYPE_STATIC) {
			return self::$types[$page['type']];
		}
		if ($page['type'] == self::TYPE_MODULE) {
			return Module::find($page['module_id'])->title;
		}
		if ($page['type'] == self::TYPE_CUSTOM) {
			return CustomPage::find($page['custom_page_id'])->title;
		}

	}

	/**
	 * Set current page in self::$tree
	 * @param $id
	 */
	public static function setCurrentPage($id) {
		array_map(function($item) use($id) {
			if ($item->id == $id) {
				$item->current = true;
			}
			return $item;
		}, self::$tree);
	}

	/**
	 * Check if page is active
	 * @param Page $page
	 * @return bool
	 */
	public static function isPageActive(Page $page) {

		if ($page->url_full === Request::getRequestUri()) {
			return TRUE;
		}

		//\FB::log( Request::getRequestUri(), $page->url_full );
		return strpos(Request::getRequestUri(), '/' . $page->url_full) === 0;
	}


	/**
	 * Get tree elements by depth
	 * @param int $depth
	 * @return array
	 */
	public static function getTreeByDepth($depth = 0) {
		return array_filter(self::$tree, function($item) use ($depth) {
			return $item['level'] == $depth;
		});
	}

	/**
	 * Get first child by parent_id
	 * @param $parent_id
	 * @return bool
	 */
	public static function getFirstChild($parent_id) {

		foreach(self::$tree as $item) {
			if ($item->parent_id == $parent_id) {
				return $item;
			}
		}

		return FALSE;

	}

	/**
	 * Get tree elements by depth
	 * @param $parent_id
	 * @internal param int $depth
	 * @return array
	 */
	public static function getTreeByParent($parent_id) {
		return array_filter(self::$tree, function($item) use ($parent_id) {
			return $item['parent_id'] == $parent_id;
		});
	}


	/**
	 *
	 * @param $level
	 *
	 * @return mixed
	 */
	public static function getAciveByLevel($level) {

		$tree = array_filter(self::$tree, function($item) use($level) {
			return $item->level == $level && $item->active;
		});

		return array_shift($tree);

	}

	/**
	 * Get children of page
	 */
	public function getChildren() {
		return self::getTreeByParent($this->id);
	}

	/**
	 * Check if page has children
	 */
	public function haveChildren() {
		$children = self::getTreeByParent($this->id);
		return !empty($children);
	}


	/**
	 * Get whole page tree
	 *
	 * @return array
	 */
	public static function getPageTree() {

		if (empty(self::$tree)) {
			self::loadPageTree();
		}

		return self::$tree;

	}

	/**
	 * Get current path array
	 */
	public static function getPathArray() {

		$page = PublicController::$page;

		$path = self::_getPathArray($page->parent_id, array($page));

		return array_reverse($path);

	}

	/**
	 * Helper for self::getPathArray
	 */
	private static function _getPathArray($parent_id, $path) {

		foreach(self::$tree as $item) {
			if ($parent_id == $item->id) {
				$path[] = $item;
				return self::_getPathArray($item->parent_id, $path);
			}
		}

		return $path;
	}


	/**
	 * Check if page has children
	 * @param $parent_id
	 * @return bool
	 */
	public static function hasChildren($parent_id) {

		foreach(self::$tree as $item) {
			if ($item->parent_id == $parent_id) {
				return TRUE;
			}
		}

		return FALSE;

	}

	/**
	 * Get page depth
	 * @param object $tree
	 * @param object $page
	 * @param int $current
	 * @return int
	 */
	private static function getTreeLevel($tree, $page, $current = 0) {

		foreach($tree as $item) {
			if ($page->parent_id == $item->id) {
				return $item->parent_id ? self::getTreeLevel($tree, $item, ++$current) : ++$current;
			}
		}

		return $current;

	}

	/**
	 * Get page full URL
	 * @param object $tree
	 * @param object $page
	 * @param string $url
	 * @return int
	 */
	private static function getPageFullURL($tree, $page, $url = '/') {

		foreach($tree as $item) {

			if ($page->parent_id == $item->id) {

				$url = $item->url . '/' . $url;

				if ($item->parent_id) {
					return self::getPageFullURL($tree, $item, $url);
				}
				else {
					return $url;
				}

			}
		}

		return $url;

	}


	/**
	 * Before save
	 * @return bool
	 */
	public function onSave() {

		if (empty($this->url)) {
			$slugify = new Slugify;
			$this->url = $slugify->slugify($this->getMultiAttribute('title'));
		}

		if ($this->type == Page::TYPE_STATIC) {
			$this->module_id = NULL;
			$this->custom_page_id = NULL;
		}
		if ($this->type == Page::TYPE_MODULE) {
			$this->content = NULL;
			$this->custom_page_id = NULL;
		}
		if ($this->type == Page::TYPE_CUSTOM) {
			$this->module_id = NULL;
			$this->content = NULL;
		}

		return TRUE;

	}


	/**
	 * Get array of nested menus
	 */
	public static function getNested() {

		$pages = self::orderBy('ord', 'ASC')->get();
		$refs = array();
		$list = array();

		foreach ($pages as $page) {

			$ref = &$refs[$page->id];

			$ref['parent_id'] = $page->parent_id;
			$ref['title'] = $page->title;
			$ref['title'] = $page->title;
			$ref['custom_page_id'] = $page->custom_page_id;
			$ref['module_id'] = $page->module_id;
			$ref['ord'] = $page->ord;
			$ref['home'] = $page->home;
			$ref['url'] = $page->url;
			$ref['status'] = $page->status;
			$ref['menu'] = $page->menu;
			$ref['type'] = $page->type;
			$ref['id'] = $page->id;

			if ($page->parent_id == 0) {
				$list[$page->id] = &$ref;
			} else {
				$refs[$page->parent_id]['children'][$page->id] = &$ref;
			}

		}

		return $list;

	}

	/**
	 * Save page order from array
	 * @param array $data
	 * @param int $parent_id
	 * @return bool
	 */
	public static function savePageOrder($data, $parent_id = 0) {

		foreach($data as $ord => $item) {

			\DB::table('pages')
				->where('id', '=', $item['id'])
				->update(array(
					'ord' => $ord,
					'parent_id' => $parent_id,
				));

			if (!empty($item['children'])) {
				self::savePageOrder($item['children'], $item['id']);
			}

		}

		return TRUE;

	}

}