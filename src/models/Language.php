<?php namespace Simple\Cms;

use \Eloquent;
use Symfony\Component\HttpFoundation\Request;

class Language extends Eloquent {

	/**
	 * Status enabled
	 */
	const STATUS_ENABLED = 1;

	/**
	 * Status disabled
	 */
	const STATUS_DISABLED = 1;

	/**
	 * Table
	 * @var string
	 */
	protected $table = 'languages';

	/**
	 * All languages
	 * @var array
	 */
	public static $languages = array();

	/**
	 * Default language
	 * @var string
	 */
	public static $default = 'en';

	/**
	 * Fillable fields
	 */
	protected $fillable = array('title', 'code', 'status');




}