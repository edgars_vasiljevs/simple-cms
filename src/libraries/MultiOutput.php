<?php namespace Simple\Cms;

use Config;
use Exception;

class MultiOutput {

	public $values;

	/**
	 * Initialize class
	 * @param stdClass $values
	 */
	public function __construct($values) {
		$this->values = $values;
	}

	/**
	 * Output specific language
	 * @param string $locale
	 * @return mixed
	 * @throws Exception
	 */
	public function __get($locale) {

		if ( ! isset($this->values->{$locale})) {
			throw new Exception('Variable not found');
		}

//		if (is_object(json_decode($this->values->{$locale}))) {
//			return new self($this->values->{$locale});
//		}

		return $this->values->{$locale};

	}

	/**
	 * Output default language
	 * @return mixed
	 * @throws Exception
	 */
	public function __toString() {

		$locale = Config::get('app.locale');

		if ( ! isset($this->values->{$locale})) {
			throw new Exception('Variable not found');
		}

		return $this->values->{$locale};

	}

}