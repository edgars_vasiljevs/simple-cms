<?php namespace Simple\Cms;

use Illuminate\Routing\Router;
use Illuminate\Container\Container;

class Controller extends \Illuminate\Routing\Controllers\Controller {

	/**
	 * Template variable
	 */
	protected $template;

	/**
	 * Execute an action on the controller.
	 *
	 * @param  \Illuminate\Container\Container  $container
	 * @param  \Illuminate\Routing\Router  $router
	 * @param  string  $method
	 * @param  array   $parameters
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function callAction(Container $container, Router $router, $method, $parameters)
	{
		$this->filterParser = $container['filter.parser'];

		// If no response was returned from the before filters, we'll call the regular
		// action on the controller and prepare the response. Then we will call the
		// after filters on the controller to wrap up any last minute processing.
		$response = $this->callBeforeFilters($router, $method);

		$this->setupLayout();

		if (is_null($response))
		{
			$response = $this->callMethod($method, $parameters);
		}

		// If no response is returned from the controller action and a layout is being
		// used we will assume we want to just return the layout view as any nested
		// views were probably bound on this view during this controller actions.
		if (is_null($response) and ! is_null($this->template))
		{
			$response = $this->template;
		}

		if (is_null($response) and ! is_null($this->layout))
		{
			$response = $this->layout;
		}

		return $this->processResponse($router, $method, $response);
	}

}