<?php namespace Simple\Cms\Validation;

/**
 * Validator service
 */
abstract class Validator {

	/**
	 * Attributes
	 */
	protected $attributes;

	/**
	 * Multi language fields
	 */
	public static $multi = array();

	/**
	 * Store errors messages
	 */
	public $errors;

	public function __construct($attributes = NULL) {

		$this->attributes = $attributes ? : \Input::all();

	}

	/**
	 * Check if validation passes
	 */
	public function passes() {

		$multi_rules = self::convertMultiFields();

		$validation = \Validator::make($this->attributes, $multi_rules);

		if ($validation->passes())
			return TRUE;

		$this->errors = $validation->messages();

		return FALSE;

	}

	/**
	 * Convert multi language fields to correct rules
	 */
	public static function convertMultiFields() {

		$rules = static::$rules;

		foreach (static::$multi as $multi) {

			if (isset($rules[$multi])) {

				$rule = $rules[$multi];

				unset($rules[$multi]);

				foreach (\Simple\Cms\Language::$languages as $lang) {

					$rules[$multi . '_' . $lang->code] = $rule;

				}

			}

		}

		return $rules;

	}

}