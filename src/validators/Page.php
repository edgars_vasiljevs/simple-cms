<?php namespace Simple\Cms\Validation;


class Page extends Validator {

	public static $rules = array('title' => 'required', 'layout_id' => 'exists:layouts,id', 'type' => 'in:1,2,3');

	public static $multi = array('title', 'content');

}