<?php namespace Simple\Cms\Validation;


class Language extends Validator {

	public static $rules = array(
		'title' => 'required',
		'code' => 'required',
		'status' => 'in:0,1'
	);

}